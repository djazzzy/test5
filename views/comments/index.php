<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">


    <div class="comments-list">
        <?

        foreach ($array as $arr) { ?>
        <div class="comment-item">
            <div class="comment-item__top">
                <div class="comment-item__name"><? echo $arr->name; ?></div>
                <div class="comment-item__date"><? echo $arr->date; ?></div>
            </div>
            <div class="comment-item__text">
                <? echo $arr->text; ?>
            </div>
        </div>
            <?
        }
        ?>
    </div>

    <div class="comment-add">
        <div class="comment-add__title">Оставить комментарий</div>
        <?php $form = ActiveForm::begin([
            'options' => [
            'class' => 'userform'
                ]
        ]); ?>

        <div class="comment-add__name">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="comment-add__text">
            <?= $form->field($model, 'text')->textarea(['rows' => 8]) ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'comment-add__btn']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>





</div>
