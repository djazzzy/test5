<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <div class="header content-size">
        <div class="header__left">
            <div class="header-contact">
                <div class="header-contact__phone">Телефон: (499) 340-94-71</div>
                <div class="header-contact__mail">Email: info@future-group.ru</div>
            </div>
            <div class="header__title">Комментарии</div>
        </div>
        <div class="header__rigth">
            <img src="../../img/logo.png" alt="" class="header__img">
        </div>
    </div>

    <div class="content-wrap content-size">
        <?= $content ?>
    </div>
</div>

<footer class="footer content-size">
    <div class="footer-wrap">
        <div class="footer__logo"><img src="../../img/logo_footer.png" alt="" class="footer_img"></div>

        <div class="footer-content">
            <div class="footer__contact">
                <div class="footer__contact-item">Телефон: (499) 340-94-71</div>
                <div class="footer__contact-item">Email: info@future-group.ru</div>
                <div class="footer__contact-item">Адрес: 115088 Москва, ул. 2-я Машиностроения, д. 7 стр. 1</div>
            </div>
            <div class="footer__copyr">© 2010 - 2014 Future. Все права защищены</div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
